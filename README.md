# Vigigloo Digest Script

## Introduction

Le script `vigigloo.yml` a été créé dans le but de fournir un récapitulatif quotidien des tags modifiés ou nouvellement créés pour une liste spécifiée de dépôts, permettant ainsi à l'équipe technique de rester informée des mises à jour importantes. Ce script est configuré pour s'exécuter quotidiennement via un job CI (Intégration Continue) et publier les résultats dans un canal Mattermost dédié.

## Fonctionnement

1. **Récupération des Repositories**:
   Le script commence par lire la liste des dépôts mentionnés dans le fichier YML.

2. **Extraction des Tags**:
   Il parcourt chaque dépôt et interroge l'API Git pour obtenir la liste des tags, en identifiant ceux qui ont été créés ou modifiés depuis la dernière exécution.

3. **Compilation du Digest**:
   Un récapitulatif est compilé, listant tous les tags modifiés ou nouvellement créés pour chaque dépôt.

4. **Publication sur Mattermost**:
   Le récapitulatif est ensuite publié dans le canal Mattermost spécifié, permettant à l'équipe de voir facilement les mises à jour.

## Configuration

### Variables d'Environnement

Les variables d'environnement pertinentes doivent être configurées dans le job CI. Voici les variables utilisées dans le script:

- `MATTERMOST_API_KEY`: La clef d'API pour publier sur mattermost